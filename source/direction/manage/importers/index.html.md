---
layout: markdown_page
title: "Category Direction - Importers"
---

- TOC
{:toc}

Last Reviewed: 2020-05-04

## Introduction and how you can help

Thanks for visiting the direction page for Importers in GitLab. This page belongs to the [Import](https://about.gitlab.com/handbook/product/categories/#import-group) group of the [Manage](https://about.gitlab.com/direction/dev/#manage-1) stage and is maintained by [Haris Delalić](https://gitlab.com/hdelalic) who can be contacted directly via [email](mailto:hdelalic@gitlab.com). This vision is a work in progress and everyone can contribute. If you'd like to provide feedback or contribute to this vision, please feel free to comment directly on issues and epics at GitLab.com.

## Mission

The mission of the Importers category is to provide a great experience importing from other applications in our customer's DevOps toolchains, thereby removing friction for migrating to GitLab. Our goal is to build the Importers that our customers find valuable, reliable and easy to use in order to create a more positive first impression when migrating to GitLab. This also includes GitLab-to-GitLab migrations, particularly self-managed GitLab to GitLab.com.  

## Problems to solve

A typical organization looking to adopt GitLab already has many other tools. Artifacts such as code, build pipelines, issues and roadmaps may already exist and are being used daily. Seamless transition of work in progress is critically important and a great experience during this migration creates a positive first impression of GitLab. Solving these transitions, even for the complex cases, is crucial for GitLab’s ability to expand in the market.

At this time, GitLab is targeting the following high-level areas for import: planning tools, CI/CD tools, and source control tools.

In addition to the migration from other tools, creating easy and reliable GitLab-to-GitLab migrations allows our customers to choose how they access GitLab. While Projects can be migrated using the UI, the ability to migrate GitLab Groups is only available via the API, which requires custom work and manual intervention. With GitHost being deprecated, it is even more important to have a robust migration solution that allows our customers to easily migrate to GitLab.com.

A large portion of our current Importer issues is about the reliability of the solutions. We will need to make our Importers more reliable for our customers to have confidence in the migration process. The availability of importers for our main competitors is another challenge that we will need to address. And finally, the current Importer user experience is hard to discover and is not always user friendly.

* Note: While the Import group’s main focus is Importers, other groups may choose to contribute to individual Importers based on their strategic importance and adoption of their features. This is in keeping with GitLab’s mission that [everyone can contribute](https://about.gitlab.com/handbook/values/#mission). At this time, our Jenkins and Jira importers are currently being delivered by our Verify and Plan stage teams respectively. Other Importers continue to be fully delivered by the Import group. 

## What's next & why

To provide a path for our customers moving from GitHost to GitLab.com, the Import group is currently focused on enabling GitLab.com adoption through the introduction of [Group Export/Import API](https://gitlab.com/groups/gitlab-org/-/epics/1952). Group Import’s next priority is exposing the [Group Export/Import](https://gitlab.com/groups/gitlab-org/-/epics/2888) functionality in the GitLab UI and continuing to improve the overall [import user experience](https://gitlab.com/groups/gitlab-org/-/epics/2732).  

Jira Importer and Jenkins Importer are also in focus and currently being implemented by our Verify and Plan stage teams. These two Importers were identified as top 2 IACV features for the Dev section, as these tools are GitLab’s largest competitors in their respective categories.

Finally, the Import group will continue to work on the maintenance and improving the stability of other Importers, such as GitHub and BitBucket.

At the moment, the Import group is focused on enabling GitLab.com adoption through the introduction of [group import/export](https://gitlab.com/groups/gitlab-org/-/epics/1952). Additionally, our UX group is looking into the overall import user experience in order to create a more positive first impression when migrating to GitLab.

## What is not planned right now

Group Import continuously evaluates and updates the Importers' direction and roadmap. As part of that effort, new Importers such as Trello are being discussed. While these discussions may ultimately lead to the implementation of a new feature or a new Importer, none of them are being planned at this time.

If you'd like to contribute feedback on areas you'd like to see prioritized, please add them as comments in the corresponding [epic](https://gitlab.com/groups/gitlab-org/-/epics/2721) for this category.
