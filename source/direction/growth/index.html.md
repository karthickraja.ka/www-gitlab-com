---
layout: markdown_page
title: Product Direction - Growth
description:
---

## Growth Section Overview

The Growth section at GitLab was formed as of August 2019 and we are iterating along the way. We are excited by the unique opportunity to apply proven growth approaches and frameworks to a highly technical and popular open core product. Our goal is to accelerate and maximize GitLab user and revenue growth, and while doing that, to become a leading B2B product growth team and share our learnings internally and externally.

Currently, our Growth section consists of 6 groups (acquisition, conversion, retention, expansion, fulfillment, telemetry), each consisting of a cross-functional team of Growth Product Managers, Developers, UX/Designers, with shared analytics, QA and user research functions.

### Growth Section Principles

In essence, we are a product team with a unique focus and approach. 

##### Principle 1: The Growth section focuses on connecting our users with our product value

While most traditional product teams focus on creating value for users via shipping useful product features, the Growth section focuses on connecting users with that value.  For example, we do so by: 
* Driving feature adoption by removing barriers and providing guidance
* Lowering Customer Acquisition Cost (CAC) by maximizing the conversion rate across the user journey
* Increasing Life time value (LTV) by increasing retention & expansion
* Lowering sales/support cost by using product automation to do the work

##### Principle 2: The Growth section sits in the intersection of Sales, Marketing, Customer success & Product

Growth Teams are by nature cross-functional as they interact and collaborate closely with many other functional teams, such as Product, Sales, Marketing, Customer success etc. Much of growth team's work helps to complement and maximize these team's work, and ultimately allow customers to get value from our product. 

![growth team helps](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/ece3ead382dbd91201f8e09246ecdc5c6c415643/source/images/growth/growthhelps.png)


To provide some examples:
* While the Marketing Team works on creating awareness and generating demand via channels such as webinars, content marketing & paid ads, the Growth team can help lower friction in signup and trial flows to maximize the conversion rate, thus generating higher ROI on marketing spend
* While the Sales Team works on converting larger customers in a high touch manner,the Growth team can help create self-serve flows and funnels to convert SMB customers, as well as building automated tools to make the sales process more efficient
* While the Core product Team works on building features to solve customer problems and pain points, the Growth team can help drive product engagement, create usage habit and promote multi-feature adoption
* While the Customer Success & Support Teams work on solving customers problems and driving product adoption via human interaction, the Growth Team can help build new user onboarding flows and on-context support to serve some of these needs programmatically at large scale

##### Principle 3: The Growth Section uses a data-driven and systematic approach to drive growth

Growth teams use a highly data & experiment driven approach
* We start from clearly defining what success is and how to measure it, by picking a [North Star Metric](https://about.gitlab.com/handbook/product/metrics/#north-star-metric) - the one metric that matters most to the entire business now
* Then we break down the NSM into sub-metrics, this process is called building a growth model. We continuously iterate our thinking about GitLab's growth model as we learn more about our business, but our latest thinking is captured here [GitLab Growth Model](https://docs.google.com/presentation/d/1aGj2xCh6VhkR1DU07Nm3oaSzqDLKf2i64vlJKjn05RM/edit?usp=sharing). 

![GitLab Growth Model](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/0303312efae07608a8b6077996041573637d4825/source/images/growth/growthmodel.png)


* Then by reviewing the sub-metrics, we identify the area with highest potential for improvement, and pick a focus area KPI for that  
* Then we use the build-measure-learn framework to test different ideas that can potentially improve the focus area  KPI. We utilize an experiment template to capture the hypothesis all the way to the experiment design, and then rank all experiment ideas using ICE framework.
* Once this focus area KPI has been improved, we go back to the growth model, and identify the next area we want to focus on 

By following this systematic process, we ensure that: 1) We know what matters most; 2) Teams can work independently but their efforts will all contribute to overall growth ; 3) We always work on the projects with the highest ROI at any given moment
##### Principle 4: The Growth Section experiments a lot and has a “Win or Learn”  mindset

***"Experiments do not fail, hypotheses are proven wrong"***

Growth teams view any changes as an “experiment”, and we try our best to measure the impact. With well-structured hypotheses and tests, we can obtain valuable insights that will guide us towards the right direction, even if some of the experiments are not successful. 

![GitLab Growth Process](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/0303312efae07608a8b6077996041573637d4825/source/images/growth/growthflow.png)

Below is a sample list of our currently running and concluded experiments. 

| Experiment Name | Group | Status | Issue Link | Results Summary |
| ------ | ------ |------ |------ |------ |
|Display security navigation in left sidebar to a cohort of net new .com signups|Conversion|Live|https://gitlab.com/gitlab-org/gitlab/-/issues/34910| NA|
|Update .com paid sign up flow|Acquistion|Live|https://gitlab.com/gitlab-org/growth/product/issues/87|  NA|
|MR with no pipepline|Expansion|Live|https://gitlab.com/gitlab-org/growth/product/issues/176| NA|
|Auto renew toggle vs. Cancel |Retention |Concluded|https://gitlab.com/gitlab-org/growth/product/-/issues/43 |https://gitlab.com/gitlab-org/growth/product/issues/162|




## Growth Section's Current Focus

### FY21 Q2 Growth Section OKR

EVP, Product: Drive incremental IACV through growth, pricing, and fulfillment improvements [Epic 484](https://gitlab.com/groups/gitlab-com/-/epics/484)
1. EVP, Product KR1: Increase customer satisfaction score of billing-related business processes to > 95+  (Owner)
1. EVP, Product KR2: Complete subscription model update including thorough communication and company-wide support plan (Supporting)
1. EVP, Product KR3: Drive $615k in incremental IACV (rolling 90 days) through growth experiments  (Owner)

EVP, Product: EVPP KR1: Increase Stages per Active Namespace [Epic 485](https://gitlab.com/groups/gitlab-com/-/epics/485)
1. EVP, Product KR1: EVPP KR1: Increase Stages per Active Namespace for namespaces less than 90 days old to >1.7 (up from 1.4 baseline) (Supporting)
1. EVP, Product KR2: Implement North Star metrics for 100% of groups and dashboard metrics for 50% of groups  (Owner)


Please refer [here](https://about.gitlab.com/company/okrs/fy21-q2/) for latest GitLab Company OKR



## Growth Section's 1-Year Plan

In 2020, Covid-19 has brought uncertainty and disruptions to the market. In tough economic times, all businesses need to focus on efficient growth. GitLab's Growth Section is perfectly positioned to help the company drive growth while improving efficiency in all fronts. Therefore we've aligned our 1-Year Growth Team Strategy into the 3 themes below: 

##### Theme 1: Improve customer retention to build a solid foundation for growth

Existing customer retention is our life blood, especially in tough marco-economic environments. GitLab has a unique advantage with a subscription business model, however we still need to make sure we serve our customers well and minimize customer churn. On the other hand, in tough times, our customers will potentially be facing challenge and uncertainty as well, therefore it will be naturally harder to upsell them with higher tiers or more seats. By staying laser focused on retention, we can build a strong foundation to weather the storm and drive growth sustainability. 

To improve retention, the Growth team has identified and will be working on the opportunities below: 


1) Continue to make the customer purchasing flows such as renewal and billing process robust, efficient, transparent and smooth, as these are critical customer moments.  
We have done projects such as: 
* Clarifying the options between “auto-renewal” and “cancel” 
* Providing clear information on customer licenses and usage 
* Automating renewal processes to minimize sales team burden

2)  Deep dive into the customer behavior and identify leading indicators for churn. This will include projects such as:  
* Creating usage based retention curve with data analytics team
* Capturing and analyzing [reasons for cancellation](https://app.periscopedata.com/app/gitlab/572742/Auto-Renew-AB-Test-Result?widget=7523649&udv=0) 
* Experimenting with different types of “early interventions” to reduce churn

3) Identify and experiment on drivers that can lead to better retention. 

Through analysis, we can identify which customer behaviors are critical to retention, and therefore we can experiment on prompting these types of behaviors among more customers. For example,  one hypothesis is that if a customer uses multiple stages and features of GitLab, they are more likely to get value from a single DevOp platform, thus they are more likely to become a long term customer. Therefore, in FY21, one of the Growth Section's focus areas will be on helping GitLab customers adopt more stages, and we'll observe if that leads to better retention to confirm the causation. 

```mermaid
graph LR
    Create --> Plan
    Verify --> Secure
    Create --> Manage 
    Create --> Verify
    Verify --> Release
    Verify --> Package
    Release --> Monitor
    Release --> Configure
    Release --> Defend
```


##### Theme 2: Increase new customer conversion rate to maximize growth efficiency

In order to grow efficiently,  we also want to maximize the efficiency of new customer acquisition. For each marketing dollar we spend, we want to bring more dollars back in terms of revenue, and we also need to reduce payback time to generate cash flow that is essential for weathering storms.

As a collaborator to marketing & sales teams, the role the growth team can play here is to aggressively analyze, test and improve the new user in- product experience between about.gitlab.com, GitLab.com, customer portal etc., with the goal of converting a prospect who doesn’t know much about GitLab, to a customer who understands the value of GitLab and happily signs up for a paid plan. 

In order to achieve this goal, we try to understand what the drivers are leading to new customer conversion and positively influence them. For example, we have identified through analysis that the number of users in a team, as well the numbers of stages/features a team tries out, all seem to be correlated with a higher conversion rate to a paid plan.  Accordingly, we planed experiments and initiatives this area such as: 
1) Building a [new customer onboarding tutorial](https://gitlab.com/gitlab-org/growth/product/-/issues/107) to help users learn how to use different stages of GitLab 

2) Qualitative study to understand the reasons behind why some customers convert, while others don’t 

3) Setting up [customers behavior triggers](https://gitlab.com/gitlab-org/gitlab/-/issues/215081) to enable the growth marketing team to send targeted & triggered emails 



##### Theme 3: Build out infrastructures and processes to unlock data-driven growth

Data is key to a growth team’s success. The Growth team uses data & analytics daily to: define success metrics, build growth models, identify opportunities, and measure progress.  So part of our focus for FY21 will be working closely with GitLab data analytics team to build out that foundation, which include: 

1) Provide knowledge & [documentation](https://docs.google.com/presentation/d/1J0dctOP-xERo2j0WMWcaBuNqTNaaPuJTxLfIaOhNRHk/edit#slide=id.g74d58e3ba0_0_5) of currently available data sources to understand customer behaviors . Also, establish a framework and best practices to enable consistent and compliant data collection

2) Build out a Product usage metrics framework to evaluate how customers engage with certain features. For example, we successfully finished SMAU data collection and dashbaording project for all GitLab product stages in Q1, and will move on to a similar [North Star Metric project](https://gitlab.com/gitlab-org/telemetry/-/issues/374) for all GitLab product features in Q2. 

3) Build out Customer journey metrics framework to understand how customers flow the GitLab through funnel, including [end to end cross-functional reporting](https://gitlab.com/gitlab-data/analytics/-/issues/4336) spanning marketing, sales and growth.   


Along the way, we also aim to share our learnings and insights with the broader GitLab team and community, as we firmly believe growth is a whole company mission, and that success ultimately comes from constant learning & iteration & testing & sharing, as well as building a cross-functional growth enginee. 

Below is a sample of the dashboads we build and use daily. 

|Area |Dashboard/Report | Description |
| ------ | ------ |------ |
|Overall Product Usage| [SMAU dashboard](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard)  | Stage Monthly Active User trend for both .com and self managed |
|Overall Product Usage| [Stage Retention and Adoption Dashboard](https://app.periscopedata.com/app/gitlab/621735/WIP:-SMAU-Retention-and-Adoption) | How popular are the stages among GitLab customers, and how well are they retaining customers |
|Acquisition |[New Customer Acquisition Dashboard](https://app.periscopedata.com/app/gitlab/531526/Acquisition-Dashboard---Last-90-days) |Trend of acquistion of new customers |
|Conversion |[New Customer Conversion Dashboard](https://app.periscopedata.com/app/gitlab/608522/Conversion-Dashboard) | How are new users converting to customers and what are the drivers |
|Retention |[Renewal Dashboard](https://app.periscopedata.com/app/gitlab/505939/Renewals-Dashboard) | Trend on renewal and cancellation of susbscriptions|
|Retention |[Retention Dashboard](https://app.periscopedata.com/app/gitlab/403244/Retention) | Retention metrics and trend |
|Expansion | [Churn/Expansion by Segments Dashboard](https://app.periscopedata.com/app/gitlab/484507/Churn-%7C-Expansion-by-Sales-Segment) | Churn/Expansion reason and sales segment |
|Expansion | [SPAN Deep Dive Report](https://docs.google.com/document/d/1zt3uUcPJW7Y1dMgtIsi4Z1antbPFPy8fp7Fib4Ps_qw/edit?usp=sharing) |How to understand SPAN (Stage Per Average Namespace) and ideas to improve SPAN|

## Growth Section Areas of Focus

### 1. Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [Customers.GitLab](https://about.gitlab.com/handbook/engineering/projects/#customers-app)
*   [License.GitLab](https://about.gitlab.com/handbook/engineering/projects/#license-app)
*   [About.GitLab.com](https://about.gitlab.com/)

### 2. Growth Group Directions
* Acquisition Direction
* [Conversion Direction](https://about.gitlab.com/direction/conversion/)
* [Expansion Direction](https://about.gitlab.com/direction/expansion/)
* [Retention Direction](https://about.gitlab.com/direction/retention/)
* [Fulfillment Direction](https://about.gitlab.com/direction/fulfillment/)
* [Telemetry Direction](https://about.gitlab.com/direction/telemetry/)


## 3. What's next: Growth Group Issue Boards


* [Acquisition board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition)
* [Conversion board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
* [Expansion Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) 
* [Retention Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention)
* [Fulfillment Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) 
* [Telemetry Board](https://gitlab.com/gitlab-org/telemetry/-/boards) 


### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
