---
layout: handbook-page-toc
title: "E-group offsite"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The E-group offsite happens every quarter for four days.

## Goal

The goal is to have 25 or 50 minute discussions around topics that benefit from in-person dialogue, require more context and where the E-Group is likely to disagree. 
The agenda should include discussions that are:

1. Top of mind
1. Actionable
1. Impactful to the trajectory of the company
1. Cross-functional

## Attendees

1. [Executives](/company/team/structure/#executives)
1. [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/); when not possible, the [Internal Strategy Consultant](/job-families/chief-executive-officer/internal-strategy-consultant/) 
1. [CEO Shadows](/handbook/ceo/shadow/)
1. [Executive Assistant](/job-families/people-ops/executive-assistant/) to the CEO (optional)

#### Roles

**Executives** are committed to working through difficult discussions and problems during the event. 
They commit to being active participants by proposing topics to the agenda before hand and being executive sponsors for those discussion topics.
They establish criteria for success for the discussion topics by, for example, identifying decisions to be made or establishing clarity on what the next steps need to be. 
If decisions need to be made to stop, continue, or change discussion when there is disagreement on path forward, the decision is responsibility of the topic executive sponsor.

The **CEO Shadows** are responsible for [taking thorough notes](/handbook/ceo/offsite/#notes) throughout the event, so that the E-Group can be focused on the discussion. 
CEO Shadows will also tasked with making [Merge Requests](/handbook/ceo/offsite/#document-during-meeting) on behalf of an Executive. 
Please follow the below outlined process for announcing and merging the changes. 

The **Chief of Staff** or other team member is responsible for facilitating the event. 
They will work with the Staff EBA closely to ensure the event runs smoothly. 
The CoS is the on-the-ground person ensuring that the event is kept on-schedule, discussions are kept on-subject, helping steer the conversation when necessary, guiding conversations towards action items, and ensuring that [implementation is about 50% of time](/handbook/ceo/offsite/#timeline). 

The **Staff Executive Business Administrator to the CEO** is responsible for organizing and coordinating the Offsite, including travel, lodging, agendas, and meals.

## Logistics

Since most of the E-group is in the San Francisco Bay Area, we'll go to a location that is drivable or a short direct flight, for example: East Bay, Denver, Sonoma. 
We tend to pick a location at or close to a member of the e-group. 
If the CEO is traveling, the CoS (preferred) or the CEO Shadows (backup) should bring [the Owl](https://www.owllabs.com). 

[Hybrid calls are hard](/handbook/communication/#hybrid-calls-are-horrible), but occassionally the Offsite will need to take a hybrid meeting form. 
When this is the case, the Staff. EBA to the CEO will ensure that the calendar invite for the Zoom offsite includes a Zoom link.
The Zoom link should have a waiting room attached to it, since the zoom URL is on calendars and is discoverable internally. 
This also allows the e-group to pull in folks as-needed into the room without switching Zoom rooms, as people won't just jump in and out without being noticed or before the e-group is ready to move onto that subject. 

#### Offsites in the era of COVID-19

COVID-19 has lead GitLab to [suspend all non-essential travel](/handbook/travel/#travel-guidance-covid-19).
That includes the e-group offsite. 

In the circumstance of a fully remote e-group offsite, instead of 2.5 days of in-person meetings, the e-group offsite occurs over 4 all-remote half-day sessions. 
This is because some people may find Zoom makes the meeting more intense.
The All-Directs follow up call will happen on the business day immediately following the final inclusion. 

## Schedule 

The off-site is a quarterly meeting scheduled over 4 days (including travel time). 
The meeting should take place during the second or third month of the quarter to avoid conflicts with Sales QBRs and ideally occur before quarterly Board of Director meetings. 
Scheduling for the event generally follows: 
1. Travel Day: travel day with optional dinner around 7pm local time (usually Monday).
1. First Day: Full day meeting starting with breakfast at 8am (usually Tuesday)
1. Second Day: Full day meeting starting with breakfast at 8am. When the event does not coincide with Contribute, Sr. EBA to the CEO will coordinate an off-site activity for the afternoon (usually Wednesday).
1. Third Day: Half day meeting starting with breakfast at 8am. Usual end time is 12:30 pm with departure flights scheduled in the late afternoon (usually Thursday).
1. First day after third day: The e-group hosts an [all-directs zoom call](#all-directs-zoom-call) (usually Friday).

E-Group is welcome to fly in early or stay later pending their travel preferences.

It generally occurs in:
* February
* May
* August
* November

In 2020, the following offsite dates have been confirmed (starting dates):
* 2020-05-26

## Timeline

1. 1 month before Book Selection is finalized
1. 2 weeks before- if a subject requires data to support the discussion, make a request of [the data team](/handbook/business-ops/data-team/) for assistance no less than 2 weeks before the offsite; [alert the data team of on-call request](/handbook/ceo/offsite/#data-team-member-on-call)
1. Friday before- agenda is finalized (though always subject to change)
1. Immediately following the Offsite, the CoST should create the agenda for the next Offsite so that topics can be added as they come up. 

## Data team member on call

A data team member is available on-call during the offsite. 
The CoS gives the data team a heads up at least 2 weeks before, so that they data team can plan appropriately.

## Agenda and Documenting

We take notes in a Google Doc that contains the SSOT of the agenda.
At least 1 month prior, the doc is created and proposed topics for discussion should be added to the bottom of the doc. 
(The CoS will pull from here when prioritizing and planning.)
Please add links to relevant materials, issues, or proposals up front.
When there is an issue or doc linked, we take notes there, instead of in the overall doc.
There is a [doc template](https://docs.google.com/document/d/1LCy1qWG88ChBXWL8YibNJbkxuxzVdh0Wx9KM1lw_Vcg/edit) that can be used as a starting point.

If we can conclude a topic early we move on to one from a reserve list.
The CoS is responsible for maintaining the schedule, optimizing discussion schedules for energy levels, and having topics prepared.

We will document agreed changes directly to the handbook and any other relevant SSoT during the meeting.
Every item will get a MR maker and MR reviewer assigned.
Most of the time the MR maker will be the CoS, one of the CEO shadows, or the Staff EBA to the CEO.
When the MR is ready, the reviewer is at-mentioned in the public e-group channel in Slack.
The reviewer communicates with the maker via that Slack thread. 
The goal is to merge it the same day, preferably within 15 minutes.

### Recurring discussion topics

There are some topics discussed at every e-group offsite. 
These include:
* Reviewing action items from the last offsite
* Dependencies outside of OKRs (2.5 hours, ~20 mins per function)
* [Talent Assessment and Succession Planning](/handbook/people-group/performance-assessments-and-succession-planning/) (1.5 hrs, ~10 mins per function)
* [Director+ Promotions](/handbook/people-group/promotions-transfers/index.html#offer-process-in-bamboohr-or-greenhouse) (as-needed based on number of people, estimate ~10 mins per person)
* [Book Discussion](/handbook/ceo/offsite/#book-choice) (1 hour)


## Follow up

Every discussion should start by clarifying "What decision needs to be made from this?"
All follow up actions need to be captured as to-dos, noted with `TODO Person` in the doc.
If there is not an exec's name tied to the to-do, it belongs to the CEO Shadows/and or the CoS. 
Before emphasizing follow ups, many conclusions never landed and/or resulted into action.
Follow ups can take the form of a/n:

1. Merge request to the handbook
1. Issue created
1. Meeting scheduled
1. Notes shared with the rest of the company in Slack
1. Etc.

## All-directs Zoom Call

On the first business day immediately following the Offsite, there is a 90-minute Zoom call before 12pm Pacific time for the [All-Directs Group](/handbook/leadership/#all-directs).
This time will not work for everyone.

The goal of this call is to communicate:
1. Strategic or visionary updates that are crucial to priorities
1. Key decisions made
1. Key messaging that leaders are enlisted to help distribute
1. Action items which may need cross-functional collaboration

The Agenda for the call is:
* Event summary 
* Decisions made
* Asks for directs
* AMA

This Zoom call is a separate invite. 
The Staff EBA to the CEO is responsible for setting up the invite, the doc for the call, and inviting all direct reports to executives.
The CoS will moderate the call.

The CoS will have materials prepared for the call, no later than 12 hours before. 
The materials used for the call can be repurposed by attendees to share key information with their teams. 
Think of the materials as a "Meeting in a Box."

## Book Choice

The offsite includes a 1 hour discussion, 15 minutes of which is on action items, on a book read explicitly for the offsite.
The CoS solicits book nominations from throughout the company into an issue.
Then the E-Group decides on the book.
The book will be finalized no less than 1 month prior to the offsite.

When possible, the book discussion will be live streamed to the GitLab Unfiltered account in order to provide transparency
and insight into action items that come from the discussion. 
Live streaming will further engage team members while promoting trust in leadership's commitment to the [GitLab values](/handbook/values).

When not possible for logistical reasons, we aim to have an AMA the first business day after the offsite with the executives on the book. 
This also allows team members to engage in the book. 

Books that the E-Group finds noteworthy should be added to the [Leadership Books](/handbook/leadership/#books).

### Previous Reads

* Better Allies by Karen Catlin
* Great at Work by Morten Hansen
* Leadership and Self-Deception by the Arbinger Institute
* Crucial Conversations by Kerry Patterson, Joseph Granny, Ron McMillan, & Al Switzler
* The Servant: A simple story about the true essence of leadership by James Hunter
* Drive: The Surprising Truth About What Motivates Us by Daniel Pink
