---
layout: handbook-page-toc
title: "Services to Accelerate Customer Adoption"
---
# Services to Accelerate Customer Adoption
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This [sales enablement](/handbook/sales/training/sales-enablement-sessions/) session is designed to help the GitLab Sales team understand our services offerings, how to [position](/handbook/customer-success/professional-services-engineering/positioning/) them to customers, and the [workflow for selling](/handbook/customer-success/professional-services-engineering/selling/) services.

### Recording

To watch a live version of the enablement sessions, please see the recording below:

Sales Enablement Level Up Webcast - April 9, 2020

[Professional Services Offerings & Positioning](https://youtu.be/_04S2JhVZ5A)

<figure class="video_container">
 <iframe src="https://youtu.be/_04S2JhVZ5A" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


### Enablement Deck
To view the enablement deck used in this presentation, please visit this [internal link](https://docs.google.com/presentation/d/1p5QweQaqqliLRTIgTo-TyicwAfEYHqAvTRUpOAt2yqs/edit#slide=id.g29a70c6c35_0_68)

## Learning Objectives

1. Why sell Professional Services?
1. Services Portfolio 
1. Current state & improvements
1. Services: Positioning and Process
1. Where to go for help

## Professional Services: What, Why and How  

### What
{:.no_toc}

Accelerate our customers’ DevOps Transformation by delivering services to improve operational efficiency, accelerate time to market and reduce risk 
through GitLab product adoption

### Why GitLab Professional Services?
{:.no_toc}

#### Customer
{:.no_toc}

* Faster value realization
* Improved customer experience
* Access to GitLab experts / best practices

#### GitLab
{:.no_toc}

* Improved retention and expansion
* Customer insights and feedback
* Experience and offers for partners

### How will we deliver?
{:.no_toc}

1. Direct via GitLab resources
1. Partner delivered (future)

## Types of Service

For details in the types of services we offer, see [our framework](/handbook/customer-success/professional-services-engineering/framework)

## How to order

Ordering services is done through an ** Off-the-shelf SKU** or a **Statement of Work**.  See the [selling services workflow](/handbook/customer-success/professional-services-engineering/selling/) for more details.

## Resources

* [Services Marketing page](https://about.gitlab.com/services/)
* [Full offering catalog](https://about.gitlab.com/services/catalog)
* [Services Calculator for custom SOWs](https://services-calculator.gitlab.io/)
* [Professional Services Pitch Deck](https://docs.google.com/presentation/d/1CFR8_ZyE9r4Dk_mjoWGe4ZkhtBimSdN0pylIPu-NAeU/edit#slide=id.g3667cdc662_0_1)


## Command of Message questions to ask

### Increase Operational Efficiencies 
{:.no_toc}

* Tell me about the most important things that need to be done to make the rollout successful.
* Describe to me where you think there may be opportunities to accelerate your time-to-value?
* How much time could we save your teams by having a GitLab expert architect and deploy GitLab for you?

### Deliver Better Products Faster
{:.no_toc}

* How would the business benefit from the ability to quickly and easily migrate teams?
* Describe how your developer productivity would be impacted by education around best practices for using GitLab, git or GitLab CI/CD

### Reduce Security and Compliance Risk
{:.no_toc}

* How does your team manage the risk associated with adding a new tool or platform?
