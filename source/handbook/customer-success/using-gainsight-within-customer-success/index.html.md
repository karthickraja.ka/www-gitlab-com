---
layout: handbook-page-toc
title: "Using Gainsight within Customer Success"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Gainsight?

Gainsight is a tool for Technical Account Managers to manage the ongoing customer lifecycle.

### Key Benefits of Gainsight

Gainsight will help across several different areas within Customer Success. Some highlights include:

- Efficiency: consolidated account views (BoB, account), telemetry, Zendesk integration
- Consistency: Establish customer lifecycle process, manage and track engagement
- Visibility: health scores, risk, adoption
- Automation: process, adoption, enablement for digital journey
- Metrics and Analytics: Stage Adoption, customer health, time-to-value,
- Grow Net Retention: Success plan-driven engagement, expand plays

## Getting Started

There are two ways to access Gainsight: [through Salesforce](#access-via-salesforce) (recommended), and by [logging in directly](#logging-in-directly).

### Access via Salesforce

_Salesforce is the recommended way of accessing Gainsight, since you will have access to the full set of account information, including opportunities, subscription information, and activity._

Log in to Salesforce, and click on the "Gainsight NXT" tab at the top of the screen. If you don't see "Gainsight NXT" as a choice, you can add it by clicking the "+" sign, choosing "Customize My Tabs" and choosing Gainsight NXT from the applications list.

To hide the Salesforce header to maximize Gainsight screen space, click the "double arrow" icon in the top right corner of the Gainsight header, next to your profile icon.

### Logging in Directly

**Note: you may log in directly via Okta but you will not have subscription data, opportunity, or Salesforce activity.**

To access Gainsight directly, go to [gitlab.gainsightcloud.com](https://gitlab.gainsightcloud.com/) and when prompted for your username and password, enter _only_ your GitLab email address. As soon as it is entered, the screen should change to indicate that you are using single sign-on, and when you click "log in" it will redirect you to Okta.

- If you need to request access to Gainsight, fill out an [Access Request](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request) and have your manager add the manager approved label.
- If you need help locating the Gainsight NXT tab in Salesforce or the Gainsight Okta tile, submit an issue using the [Gainsight Request Template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

### Confirm your timezone

Once you've logged in, click the person at the top right and click on your name. Here, you can confirm your name, title, and your timezone. Timezone will automatically be PST, so you'll want to update this to your timezone to make it easier for logging events (when logging an event, it will input the current time in your selected timezone).

## Gainsight Dashboard 360

TAMs will start on their `TAM Dashboard` for a full list and overview of their customers. This includes at-risk customers, customers in renewal, health overview, open CTAs, and Stage Adoption statistics.

The C360 is the full overview on a per-customer basis. This gives the TAM a snapshot of the customer: high level attributes, health, Success Plans, activities, Zendesk tickets, and more. The goal here is to provide broad context to the TAM about their customer.

[VIDEO: TAM Dashboard and C360 video](https://www.youtube.com/watch?v=UwPXRHwVmRQ&feature=youtu.be) for an overview of the TAM Dashboard and C360.

For the fields on the C360 that TAMs need to complete:

- [Health Score](/handbook/customer-success/tam/health-score-triage/)
- [Deployment Types](/handbook/customer-success/using-gainsight-within-customer-success/deployment-types/)
- [Stage Adoption](/handbook/customer-success/tam/stage-adoption/)


## Building a Success Plan

Learn more about how to build a [Success Plan in Gainsight](/handbook/customer-success/tam/success-plans/)

## Enablement

### Videos

The videos listed here are internal to GitLab only. You will need to be logged in to the "GitLab Unfiltered" YouTube channel to access them.

- [Gainsight Overview and Demo](https://youtu.be/6NuyNSNipgc)
- [Gainsight Enablement 1](https://youtu.be/PL9shBdCMmo): health score, call logging, and creating a success plan.

## Feature Requests

New feature requests can be submitted by creating an issue and selecting the [Gainsight Request Template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

The status of Gainsight issues can be viewed on the [Gainsight issue board](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/boards/1731118?&label_name[]=gainsight).

## Frequently Asked Questions

**Q: When do the Gainsight integrations syncs happen?**  
A: **Salesforce** syncs are queued at 12AM Pacific Time (7AM UTC). The full sync typically is completed by 4AM PT (11AM UTC).  
   - "Last Activity Date" field syncs every two hours
A: **Zendesk** syncs are queued to run every day at 12AM Pacific Time (7AM UTC).

**Q: Is there a technical document for the field mapping?**
A: Yes! [Go-To-Market Technical Documentation](https://about.gitlab.com/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/).

**Q: Will the Gainsight Chrome extension automatically link the email to the appropriate account’s timeline based on the email address?**  
A: If user's email address is in Gainsight under the Account, it will auto-link. See [Email Assist](https://support.gainsight.com/Gainsight_NXT/Email_Assist) for more details.

**Q: What calls and meetings should be logged in Gainsight?**  
A: Calls and meetings with the customer should be logged in Gainsight. Logging of internal calls relating to customers is optional and based on the TAM's discretion.

**Q: Is there a way to see the tree of the parent and child account hierarchy?**  
A: The "Account Hierarachy" on the left navigation provides this.

**Q: What is the process if I see a data issue (e.g., account hierachy, ARR, etc.)?**  
A: If the data issue is in Salesforce, use Salesforce chatter and at-mention `sales-support` with the items that need to be updated and/or reviewed. If the issue is specifically Gainsight and not sourced from Salesforce, pull in CS Ops.

**Q: When logging an activity, do I need to type all my notes in there, or can I link the meeting notes, if applicable?**  
A: Link the meeting notes and, optionally, consider adding one succinct subject line for easy Timeline skimming.

**Q: How can I most efficiently update Customer Health?**  
A: On the TAM Dashboard, scroll to the Health table and click on the TAM Sentiment and Product Risk for each customer.

**Q: I see two versions of Gainsight, which one do we use?**  
A: Gainsight NXT.

**Q: Certain fields (TAM, SAL, SA, renewal info) is not editable. How do I update that?**  
A: Update account ownership in Salesforce.

**Q: I need to add Contacts to accounts. How do I do that?**  
A: For now, add those in Salesforce. We are working to [enable Contacts to be added to Gainsight](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/953).

**Q: I am receiving an error "Query API invalid response."**  
A: Try clearing your browser cache and cookies. We recommend also checking in a private browser (Incognito Mode, etc.) to verify it's a browsser configuration issue.

**Q: What are the definitions of the customer lifecycle field options on the attributes page?**  
A: Definitions are on the customer success vision page: https://about.gitlab.com/handbook/customer-success/vision/#lifecycle-stages
