---
layout: handbook-page-toc
title: "Competencies"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

GitLab has competencies as a common framework to learn things. This page is a [Single Source of Truth (SSoT)](https://docs.gitlab.com/ee/development/documentation/styleguide.html#why-a-single-source-of-truth) resource for our competencies. 

A competency is defined as a set of skills, knowledge, and behaviors that allow an individual to effectively and efficiently carry out their role. There are three key categories of competencies at GitLab: 

*  Values Competencies - a common set of behaviors and skills that reflect the organizational [values](https://about.gitlab.com/handbook/values/) needed across the workforce
*  Remote Competencies - the skills and knowledge required to perform in an all-remote enviornment
*  Functional Competencies - the skills, knowledge, and behaviors required to perform at the functional level

## Principles

1. We will re-use the same materials for different audiences by having them on a [Single Source of Truth (SSoT)](https://docs.gitlab.com/ee/development/documentation/styleguide.html#why-a-single-source-of-truth) competency page
1. We will make competencies accessible to everyone in the world, including knowledge assessments, to validate understanding a competency (via Google forms and Zapier)
1. We will work handbook first so [everyone can contribute](https://about.gitlab.com/company/strategy/#mission)

 <!-- blank line -->
 <figure class="video_container">
   <iframe src="https://www.youtube.com/embed/oXTZQpICxeE" frameborder="0" allowfullscreen="true"> </iframe>
 </figure>
 <!-- blank line -->

## How we use competencies 

The following initiatives should use the same competencies as their SSoT.
Instead of maintaining separate materials they should link back to the most relevant place where the competency is defined. For general competencies, this is likely our [values](/handbook/values) page. For role specific competencies, this is likely the [job-family](/handbook/hiring/job-families/#format) page.
For example, we should have one SSoT for how to articulate the value of GitLab.

1.  [Job family requirements](/handbook/hiring/job-families/#format)
1.  [Interview scoring](/handbook/hiring/recruiting-framework/hiring-manager/#step-12hm-complete-feedback-in-greenhousenext-steps)
1.  [Promotion criteria](/handbook/people-group/promotions-transfers/)
1.  [9 box assessments](https://www.predictivesuccess.com/blog/9-box/)
1.  [Performance/Potential criteria](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix)
1.  [Succession planning](/handbook/people-group/performance-assessments-and-succession-planning/#succession-planning)
1.  [Learning and development](/handbook/people-group/learning-and-development/)
1.  [PDPs/PIPs](/handbook/underperformance/)
1.  [Career development](/handbook/people-group/learning-and-development/career-development/)
1.  [360 reviews](/handbook/people-group/360-feedback/)
1.  [Sales training](/handbook/sales/training/)
1.  [Sales enablement sessions](/handbook/sales/training/sales-enablement-sessions/)
1.  [Field enablement](/handbook/sales/field-operations/field-enablement/)
1.  [GitLab Training tracks](/training/)
1.  [GitLab University](https://docs.gitlab.com/ee/university/)
1.  [Customer Success Skills Exchange Sessions](/handbook/sales/training/customer-success-skills-exchange/)
1.  [Professional services offerings](/handbook/customer-success/professional-services-engineering/offerings/)
1.  [Onboarding](/handbook/general-onboarding/) both general and department specific
1.  [Reseller onboarding](/handbook/resellers/onboarding/)
1.  [Learn@GitLab](https://about.gitlab.com/learn/)
1.  [Customer Success GitLab Demos platform & catalog](https://gitlabdemo.com/)
1.  [Customer Services Guided Explorations](https://gitlab.com/guided-explorations)

## Content

1. Content is in the relevant section of our handbook (with embedded videos and pictures)
1. Knowledge assessments are created in Google Forms (via Zapier [you get a certification](/handbook/people-group/learning-and-development/certifications/#how-to-create-a-certification))
1. The [leadership forum](/handbook/people-group/learning-and-development/leadership-forum/) is organized by L&D

## Structure
Some competencies are based on job-level and others are based on your role. 

Values competencies are defined by job-level. For example, the definition of competency in the GitLab value of iteration and how you demonstrate that competency are different whether you are an Intermediate Product Manager or an EVP of Product.

In general, the scope of impact and expected behaviors expands as job level increases.

| Level | Scope of Impact | Expected Behaviors | 
|-----------------|----------------------------------------|------------------------------------------|
| Associate | Own work | Learns/Develops | 
| Intermediate | Work within team | Grows/Acts | 
| Senior | Cross functional work | Models | 
| Staff/Manager | Across Teams | Implements | 
| Senior Manager | Across Sub-Departments | Fosters | 
| Director | Across Departments | Drives the framework, strategy and plans | 
| Senior Director | Across Divisions | Develops the framework and strategy | 
| VP | Across Company + External Stakeholders | Leads Changes |
| EVP/CXO | Across Company + External Stakeholders | Champions  | 

Remote Work Competencies are defined by role: team member or people leader. Team members should understand and apply remote work competencies to their daily work. People leaders should understand, apply, coach and develop team members on remote work competencies. 

## List

#### Values Competencies
Take this [quiz](https://about.gitlab.com/handbook/values/#gitlab-values-certification) to become certified in the [CREDIT](https://about.gitlab.com/handbook/values/#credit) values. Check out additional details [here](https://about.gitlab.com/handbook/values/#gitlab-values-certification).

1. [Collaboration](/handbook/values/#collaboration-competency)
1. [Results](/handbook/values/#results-competency)
1. [Efficiency](/handbook/values/#efficiency-competency)
1. [Diversity & Inclusion](https://about.gitlab.com/handbook/values/#diversity-inclusion--belonging-competency)
1. [Iteration](https://about.gitlab.com/handbook/values/#iteration-competency)
1. [Transparency](/handbook/values/#transparency-competency)

#### Remote Work Competencies 
1. [Manager of One](https://about.gitlab.com/handbook/leadership/#managers-of-one)
1. [Effective Communication](https://about.gitlab.com/handbook/communication/#effective-communication-competency) : includes Working async: [Why](/company/culture/all-remote/asynchronous/) and [How](/handbook/communication/), Well written artifacts, [Single Source of Truth](/handbook/documentation/#documentation-is-the-single-source-of-truth-ssot) and [Producing video](/handbook/communication/youtube/)
1. [Handbook first](https://about.gitlab.com/handbook/handbook-usage/#handbook-first-competency)
1. [Using GitLab](https://about.gitlab.com/handbook/using-gitlab-at-gitlab/#using-gitlab-competency): includes [Install GitLab](/install/) and [GitLab administration](/ee/administration/)

#### Functional Competencies (additional links to be added)
* [Field Competencies](https://about.gitlab.com/handbook/sales/training/field-functional-competencies/)
* [Engineering Competencies](https://about.gitlab.com/handbook/engineering/career-development/career-matrix.html#technical-competencies)

