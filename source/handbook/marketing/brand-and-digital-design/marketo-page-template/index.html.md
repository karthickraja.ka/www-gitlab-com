---
layout: handbook-page-toc
title: "Marketo page template"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand & Digital Department
{:.no_toc}

---

This will serve as documentation for [Modular landing page 7259](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).

This marketo guided template has the following modules. Each module can be toggled on or off and has options such as background color.

## Page data options

* Page title
* Page description
* Social description
* Social image

## Common module options

* (Module) color
    * highlight-black
    * highlight-gray
    * highlight-purple
    * highlight-white
    * flattop
    * flatbottom
* (Module) visibility
    * visible
    * hidden

### Nav module

Description

Screenshot

Options

* Nav logo image path
    * Normally the default value is unchanged. In the case of a uniquely branded item such as an event this might be changed.
    * This variable is specified as a URL. Generally the image should be uploaded into marketo where the URL will be provided.
* Nav logo image style
    * This can be used to override default styles such as height.
* Nav button Visibility
    * Not all nav bars need a CTA.
* Nav CTA URL
    * Where this button links to.
* Nav CTA Copy
    * What this button should say.
* Nav color
    * Defaults to empty which sets the bar to white.
    * navbar-purple
        * Note when using a purple navbar you'll want to switch the "Nav logo image path" variable to an appropriately colored logo.

## Hero module type 1

Description

Screenshot

Options

* hero image, right column
* hero copy, left column

## Hero module type 2

Description:

Screenshot

Options:

* hero image, full width
* hero copy, overlayed on image

## 2column module

This is where the form is located.

Screenshot

Options

* 2column sidebar
    * Yes sidebar
    * No sidebar
* Form description
    * This is the content in the column to the left of the form.
* Form before
    * This is where the heading or anything else goes that you want to appear above the form.
* Form after submit
    * This is the "success message".
    * Temp hide form
        * If you want to preview how the form looks after submitting it, toggle this to Hidden. Don't forget to toggle it back afterward.
    * Temp show confirm
        * If you want to preview the confirmation message, toggle this to visible. Don't forget to toggle it back afterward.

## Speaker module

This module is for a brief list of speakers as would be seen on a webcast or event. This section requires particular formatting to appear correctly.

Screenshot

Options:

* Speaker List
    * Please add a new bulleted list for each speaker, where each item in the list below is a new bullet point within each list:

```
Bulleted list
  Speaker photo
  Name
  Title
  Company
enter/return enter/return
Bulleted list
  Speaker photo
  Name
  Title
  Company
```

## Trust module

In general it's a good idea to target this messaging subject matter and logos towards your audience.

Screenshot

Options

* Trust copy before
    * This is for specifying the top copy. Default is "Trusted by DevOps Teams and Orgs of All Types".
* Trust images
    * Within this editable area, please insert only logo images, preferably grayscale SVG files using presentation attributes (not style attributes). If you have questions around this, please contact Marketing's Brand and Digital team for assistance.

## Statistics module

Similar to the bottom section of [this example landing page](https://about.gitlab.com/compare/jenkins-alternative/).

Screenshot

Options

* (currently no options)

## Value box module

Description

Screenshot

Options

* (currently no options)

## Final CTA

Currently we encourage this module to remain off by default. There may be situations where you want to add a CTA outside of the landing page scope such as "Free Trial", "Learn more", or something like that. In that case, you would enable this module's visibility.

Screenshot

Options

* Final CTA Section
    * Note that the button styles look like a link when using the editor. Just edit the link URL and text. If you click on the "edit HTML" button you'll see the relevant wrappers and classes which should remain in place.
