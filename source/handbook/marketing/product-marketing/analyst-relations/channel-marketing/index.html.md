---
layout: handbook-page-toc
title: "Channel Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## When to engage a channel partner with Channel Marketing

As this expanded program has just launched end of April 2020, a lot of these processes for how to engage are not yet documented and will be more ad hoc in nature as more Channel Account Managers (CAMs) come on board.  Channel Marketing and [Field Marketing Managers (FMMs)](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#field-marketing--channel-marketing) will be working together to identify processes and iterate on those processes for the most efficient way to work with each other, the CAMs and the Channel Partner

### Channel Marketing go-to-market (GTM) options and requirements for Select channel partners

As CAMs on board new partners there are a few things that are required to have in place for Channel Marekting to support go-to-market / lead gen activities. Our guidance from Channel leadership is to focus in on your business plan with your **Select** Track partners as that should yield the highest return on investment (ROI)

Channel Marketing will setup weekly meeting with the CAMs globally to get an understanding of what is coming down the pipeline related to onboarding and working with **Select** partners

#### When to reach out to Channel Marketing for GTM support

At a minimum you will need the following completed and signed off prior to suggesting or scheduling GTM activities with the Channel partners:
1. Partner has a fully executed contract in place
1. Partner has been fully onboarded and certified for both sales and technical
1. In most cases the partner and CAM should have a working business plan for the next 2 quarters to get a solid understanding of what and where the **Select** partner will be focusing in on (what kind of practices will they be standing up: DevOps, DevSecOps, GitOps, etc.) 

For **Select** partners, please feel free to bring Channel Marketing/Field Marketing in your business planning meeting(s) around how GTM. This will help you expand and get creative with your go-to-market activities outside of the 'let's do a webcast' together. There are many other options to support GTM lead generation and the earlier we can get an understanding of this, the more success this activities will be. 

Once you have the 3 above mentioned requirements completed and you need to assist the channel partner (**Open** or **Select**) with launching their partnership here are some ideas for guidance that you can provide :

- Partner needs support of their press release (PR) with a quote from GitLab.
    - Do not commit to who will be providing the quote for their PR, please create a [Public Relations announcement request issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#)
- Assist the partner with demand generation: 
    - Guide the partner to host a webcast/workshop on their platform and work with them on the title and abstract for them to focus on. 
    - Find a GitLab presenter (can be the CAM or Channel SA) to speak at the event. If you need assistance in finding a resource to speak at this event, please create a [Strategic Marketing request issue](https://about.gitlab.com/handbook/marketing/product-marketing/#requesting-strategic-marketing-team-helpsupport) for Channel Marketing to assist in sourcing speaker
    - If you have a GitLab presenter, [submit a request](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#requesting-social-posts-) to the GitLab social team to help organically promote the webcast from GitLab social media accounts (this is subject to approval and guidance from the social media team)
    - Guide the partner to write a blog on their website in support of the launch or joint solution.
        - In some cases, we may be able to support their blog with a quote from GitLab. If this is needed, please create a [Public Relations announcement request issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#)
        - One way to boost the readability of their blog on their webpage is to create a quick > 10 min overview video on GitLab Unfiltered with the CAM and/or Channel SA and the partner casually providing an overview of the partnership and the value we jointly are bringing to the table with the solution. See instruction for [Uploading conversations to YouTube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#uploading-conversations-to-youtube). Once the video clip is complete you can upload it to GitLab Unfiltered and the partner can embed this video into thier blog.
    - While meetups are more of an awareness opporunity than demand gen, they are a great way to create regional awareness of our joint offerings with our Channel partners. The CAM is responsible for guiding our channel partners to leverage [virtual meetups](https://about.gitlab.com/community/virtual-meetups/) and [live in-person meetups](https://about.gitlab.com/community/meetups/)
### Which channel marketing manager should I contact?

- Listed below are areas of responsibility within the channel marketing team:

  - [Tina Sturgis](/company/team/#t_sturgis), Manager, Partner and Channel Marketing
  - TBH, Senior Channel Marketing Manager
