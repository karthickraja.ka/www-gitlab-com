---
layout: job_family_page
title: "Support Management"
---

**A brief overview:**

GitLab is building an open source, single application for the entire software development lifecycle—from project planning and source code management to CI/CD, monitoring, and security.

We ended 2019 with a team of 68 Support staff. Right now, we’re in a period of growth, we’re quickly increasing the size of the [Support team](https://about.gitlab.com/handbook/support/), in step with the number of GitLab employees which broke 1000 in 2019 as well.

At GitLab, we live and breathe open source principles. This means our entire [handbook](https://about.gitlab.com/handbook/) is online, and with a few clicks, you can find the details of [future releases](https://about.gitlab.com/direction/#future-releases), check out our [past releases](https://about.gitlab.com/releases/), and see some of the [customers you’d be supporting while working here](https://about.gitlab.com/customers/).

Support Engineering Management at GitLab isn’t your typical management opportunity because of our approach to the function. Support is embedded within the Engineering department and truly operates in an environment where Support and Engineering meet. Your team will be interacting with customers daily as they encounter the difficult edge cases of running GitLab in complex environments.

In the space of a day, our engineers might be doing behind the scenes work of a Linux administrator troubleshooting performance problems by using strace on a particular process, interacting with the Product team to ensure that steps in a bug report are reproducible, or with the Engineering team itself diving deep into our codebase and putting together a merge request to actually fix a customer issue. That’s not all though - they’d equally be invited to improve our documentation, help build out more efficient support processes in our [issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) and dive into the cutting edge technologies that will define how we will do work tomorrow.

At GitLab, we hold that [managers should be technically competent and experienced in the subject matter](https://about.gitlab.com/handbook/leadership/#no-matrix-organization). So, it would be your job to use your technical expertise to support all of those moving pieces - including stepping into the shoes of a Support Engineer when needs be.

We want to live in a world where [everyone can contribute](https://about.gitlab.com/handbook/values/#mission), and as a member of the support team there are no barriers to using your skills to improve the experience of our users, our customers and importantly the folks you’d be supporting as a manager.


### What you can expect in a Support Engineering Manager role at GitLab:

As a Support Engineering Manager, you will be:

* helping hire a team of Support Engineers who are focused on delivering world class technical support.
* helping Support Engineers level up in their skills and experience.
* driving team members to be self-sufficient.
* building processes that enable team members to collaborate and execute.
* holding regular 1:1s with all members on their team.
* creating a sense of psychological safety on your team.
* engaging with our customers to triage their issues via email and video conferencing.
* creating, updating, or reviewing documentation changes based on customer interactions.
* fostering an environment for the engineers on your team to maintain good ticket performance and satisfaction.
* training Support Engineers to screen applicants and conduct technical interviews.
* improving the customer experience in measurable and repeatable ways.
* participating in escalation on-call rotation (limited to regional working hours during the week and weekend).


### Projects you might work on:

When you’re not tackling difficult customer challenges, you’ll have a lot of freedom to work on things that will make your (and the lives of your coworkers) easier. Current and past support engineers have:

* Created a tool to [quickly analyze strace output](https://gitlab.com/gitlab-com/support/toolbox/strace-parser)
* Built and maintained tooling to handle our call scheduling
* Scripted a [solution to capture the state of a customer’s server](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) for easier troubleshooting
* Added functionality to [ChatOps](https://docs.gitlab.com/ee/ci/chatops/) to make it easier to identify user accounts on GitLab.com
* Written a [Chrome Extension to route downloads from Zendesk tickets into organized folders](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router)
* … and more!

### You should apply if:

* You have an affinity for (and experience with) providing customer support, and making customers happy.
* You have more than 2 years experience leading Support Engineering teams.
* You’ve got advanced analytical and problem solving skills.
* You’ve got a keen eye for forward thinking solutions (Kubernetes, Containers, etc.).
* You enjoy solving many small problems per day.
* You’ve got 5+ years of support experience.
* Within the last 5 years, you have held a role at one company for at least 2 years.
* You understand DevOps processes and you appreciate the value technologies like Serverless and Kubernetes bring to the software deployment and development process.
* You’re able to communicate complex technical topics to customers and coworkers of varying technical skill level.
* You’ve got extensive experience building and scaling teams.  
* You demonstrate excellent spoken and written English.
* You’re experienced in creating and implementing new processes and procedures.
* You’re experienced in writing support content.
* You’ve got experience leading remote, distributed teams across geographies with a keen focus on employee development and the achievement of desired results.
* You’ve got extensive experience in leading teams to take ownership and work to manage the entire issue lifecycle, from customer, to development team, to resolution.
* You’re able to perform complex Linux System Administration tasks.
* You’ve got experience with web application development using an MVC framework (e.g. Ruby on Rails, Python, Django, PHP, Laravel).
* You’ve got experience with Git and CI/CD.
* Our [values](https://about.gitlab.com/handbook/values/) of collaboration, results, efficiency, diversity, iteration, and transparency resonate with you.


### Senior Support Engineering Manager
Senior Support Managers manage multiple teams and Staff-level individual contributors (ICs) within the Support Department. As a Senior Support Engineering Manager you will ensure that the managers and/or ICs who report to you, and the engineers they work with have everything they need to deliver world-class support to GitLab customers.

A Senior Support Engineering Manager will:
- Hire a world class team of Support Managers by actively seeking and hiring globally distributed talent who are focused on delivering world class customer support
- Conduct managerial interviews for Support Engineering candidates and train Support Engineering Managers to do said interviews
- Hold regular 1:1s with their team managers and skip-level 1:1s with all members of their team
- Mentor the managers on their team
- Ensure there is clarity around the priorities and goals in the Support Department
- Explain vision, build buy-in for direction and catalyze tactical and operational changes that result in OKR and KPI achievement
- Build a collaborative environment where strategy development is done in coordination with Support staff at all levels
- Develop, implement and review Support procedures, policies and activities
- Communicate KPI achievement and intermediate goal results with the executive team and across the wider Support team
- Work across functions with peers in other groups to facilitate the accomplishment of shared goals
- Simplify and speed up decision making by ensuring that every issue has a DRI, that every decision has data backing it up, and that every issue relates to a Support priority, goal or KPI
- Represent GitLab Support in client meetings, networking events and other official functions
- Partner with management on budget requirements and executing to established budgets

#### Requirements
- Technical credibility: Past experience as a Support Engineer or leading teams thereof
- Management credibility: Past experience (3 to 5 years) in Support Management
- Ability to understand, communicate and improve the quality of multiple teams
- Demonstrate longevity in at least one recent job
- Ability to be successful managing at a remote-only company
- Humble, servant leader

#### Nice-to-have Requirements
- Be a user of GitLab, or familiar with our company
- Prior product company experience
- Prior high-growth startup experience
- Experience working on systems at a massive (i.e. consumer) scale
- Deep open source experience
- Experience working with global teams
- Values diversity and inclusion in their leadership style

### Director of Support

The Director of Support role extends the Support Engineering Manager Role

- Hire a world class team of managers and Support Engineers to work on their teams
- Help their managers and developers grow their skills and experience
- Manage multiple teams and projects
- Hold regular skip-level 1:1's with all members of their team
- Create a sense of psychological safety on their teams
- Drive technical and process improvements
- Draft and Report quarterly OKRs
- Own the support experience across all products
- Exquisite communication: Regularly achieve consensus amongst departments
- Represent the company publicly at conferences

### VP of Support

The VP of Support role extends the Director of Support Role.

- Set the vision for GitLab's global support service
- Hire a world class team of support leaders, ops specialists, and support engineers
- Help their management team grow their skills and experience
- Manage multiple sub-departments and initiatives
- Run the monthly Support Metrics review meeting
- Hold regular skip-level meetings with all members of their department
- Create a sense of psychological safety within their department
- Drive technical and process improvements
- Drive quarterly OKRs
- Define KPIs for the Support Department _e.g._ SLAs, Customer Satisfaction
- Partner with Finance to run an efficient department at or below plan
- Work cross-functionally with other customer-facing departments to guarentee a great customer experience
- Represent the company publicly at conferences

## Performance Indicators

Support Management have the following job-family performance indicators.

* [Customer satisfaction with Support](/handbook/support/#support-satisfaction-ssat)
* [Manage team within approved operating expenses](/handbook/support/#customer-support-operating-expenses)
* [Service Level Agreement](/handbook/support/#service-level-agreement-sla)
* [Maintain hiring plan and capacity to achieve IC:Manager ratio](/handbook/support/#individual-contributor-to-manager-ratio)
* [Develop a team of experts](/handbook/support/#increase-capacity--develop-experts)

## Span of Control
Support Management roles have the following spans of control:
- Support Managers: ~10 Intermediate and Senior Support Engineers
- Senior Support Managers: ~3 Support Managers, 0-2 Staff Support Engineers, ~30 Support Engineers (indirectly)

### [What it’s like to work here](https://about.gitlab.com/jobs/faq/#whats-it-like-to-work-at-gitlab) at GitLab:

The [culture](https://about.gitlab.com/company/culture/) here at GitLab is something we’re incredibly proud of. Because GitLabbers are currently located in over 51 different countries, you’ll spend your time collaborating with kind, talented, and motivated colleagues from across the globe. Some of the [benefits](/handbook/total-rewards/benefits/) you’ll be entitled to vary by the region or country you’re in. However, all GitLabbers are fully remote and receive a "no ask, must tell" paid-time-off policy, where we don’t count the number of days you take off annually. You can work incredibly flexible hours, enabled by our [asynchronous approach](https://about.gitlab.com/handbook/communication/) to communication. We’ll also help you set up your [home office environment](https://about.gitlab.com/handbook/spending-company-money/), pay for your membership to a co-working space, and contribute to the [travel costs](https://about.gitlab.com/handbook/incentives/#visiting-grant) associated with meeting other GitLab employees across the world. 

Also, every year or so, we’ll invite you to our [Contribute event](https://about.gitlab.com/company/culture/contribute/).

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a 30min screening call with our Global Recruiters.

* Selected candidates will receive a short questionnaire from our Global Recruiters

* Next, candidates will move to the first round of interviews
  * 60 Minute Peer Behavioral Interview (Panel)
  * 90 Minute Technical Interview with a member of the Support team
    - The Tech Interview will involve live break-fix/bug-fix scenarios as well as customer scenarios.  You will need to have access to a terminal with Bash or similar. You will also need to have an SSH key pair installed locally so you can connect to the server. Windows users must have ‘Git Bash for Windows’ installed prior to the call. If the Tech Interview is not passed, the Behavioral Interview will be canceled.
* Next, candidates will move to the second round of interviews
  * 60 Minute Interview with the Senior Support Manager in that region
  * Some candidates will be asked to join a 60 Minute Interview with the Director of Support
* Successful candidates will subsequently be made an offer

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).
