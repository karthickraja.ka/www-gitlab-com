---
layout: job_family_page
title: "Product Strategy"
---

## Product Strategy Team

The Product Strategy team is responsible for delivering long-range impact, specifically focused on [interactive prototypes] and [landing moonshots].
The team reports to the VP of Product Strategy.

[interactive prototypes]: /handbook/product-strategy/#interactive-prototypes
[landing moonshots]: /handbook/product-strategy/#landing-moonshots

## Responsibilities

* Create [interactive prototypes] of the future; telling a story that conveys our vision and strategy.
* Cut down [moonshots](/direction/#moonshots) into iterable issues; deliver first MVCs.

## Fullstack Engineer, Product Strategy

#### Requirements
* Full-stack developer with significant professional experience with Ruby on Rails and Javascript
* Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment
* Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
* Comfort working in a highly agile, [intensely iterative][iteration] software development process
* Demonstrated ability to onboard and integrate with an organization long-term
* Positive and solution-oriented mindset
* Effective communication skills: [Regularly achieve consensus with peers][collaboration], and clear status updates
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* [Self-motivated and self-managing][efficiency], with strong organizational skills.
* Demonstrated ability to work closely with other parts of the organization
* Share [our values][values], and work in accordance with those values
* Ability to thrive in a fully remote organization
* Ability to use GitLab

[values]: /handbook/values/
[collaboration]: /handbook/values/#collaboration
[efficiency]: /handbook/values/#efficiency
[iteration]: /handbook/values/#iteration

#### Nice-to-haves

* Experience in a peak performance organization, preferably a tech startup
* Experience with the GitLab product as a user or contributor
* Product company experience
* Experience working with a remote team
* Enterprise software company experience
* Developer platform/tool industry experience
* Experience working with a global or otherwise multicultural team
* Computer science education or equivalent experience
* Passionate about/experienced with open source and developer tools

## Designer, Product Strategy

## Responsibilities

* Help to define and improve the interface and experience of GitLab.
* Design features that fit within the larger experience and flows.
* Create deliverables (wireframes, mockups, prototypes, flows, etc.) to communicate ideas.
* Work with Product Managers and Engineers to iterate on and refine the existing experience.
* Involve UX Researchers when needed, and help them define research initiatives (usability tests, surveys, interviews, etc.).
* Stay informed and share the latest on UI/UX techniques, tools, and patterns.
* Understand responsive design and best practices.
* Have working knowledge of HTML, CSS. Familiarity with Javascript.
* Have knowledge and understanding of design systems theory and practice.
* Have a general knowledge of Git flow (feature branching, etc.), merge/pull requests, pipelines, and code testing.

### Product Designer

* Deeply understand the technology and features of the stage group to which you are assigned.
* Actively evaluate and incorporate feedback from UX Research, and conduct evaluative research with guidance from the UX Research Team and/or Senior Product Designers.
* Help create strategic deliverables like journey maps, storyboards, competitive analyses, and personas.
* Create well-designed tactical deliverables, like wireframes and prototypes, to communicate your ideas.
* Proactively identify both small and large usability issues within your stage group.
* Participate in Design Reviews, giving and receiving feedback in an appropriate way.
* Actively contribute to the [Pajamas Design System][pajamas].
* Break down designs to fit within the monthly release cadence, engage your team to review designs early and often, and productively iterate across milestones. Prioritize your tasks, even when deadlines seem overwhelming, and ask for help, as needed.
* Participate in and help drive [product discovery](https://about.gitlab.com/handbook/product/product-management/process/#product-discovery-issues) to generate ideas for features and improvements.
* Take part in the monthly release process by reviewing and approving UX implementations.

### Senior Product Designer

Everything in the Product Designer role, plus:

* Have working knowledge of the end-to-end GitLab product.
* Proactively identify strategic UX needs within your stage group, and engage other Product Designers within your stage group to help you create deliverables like journey maps, storyboards, competitive analyses, and personas.
* Proactively identify both small and large usability issues within your stage group, and help influence your Product Manager to prioritize them.
* Proactively identify user research needs, and conduct evaluative research with guidance from the UX Research team.
* Broadly communicate the results of UX activities within your stage group to the UX department, cross-functional partners within your stage group, and other interested GitLab team-members using clear language that simplifies complexity.
* Mentor other members of the UX department, both inside and outside of your stage group.
* Engage in social media efforts, including writing blog articles, giving talks, and responding on Twitter, as appropriate.
* Interview potential UX candidates.

## Performance Indicators

* Interactive prototypes created
* Moonshots landed

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 60 minute interview with the VP of Product.
* Next, candidates will be invited to schedule one or more 45 minute interviews with Engineering  Managers.
* Next, candidates may be invited to a 90 minute deep-dive exercise.
* Successful candidates will subsequently be made an offer.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
