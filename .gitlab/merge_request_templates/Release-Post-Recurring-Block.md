The Release Post Manager will use this template to create separate performance improvement and bug fixes MRs for the release post blog.

## Key dates & Review

- [ ] Between the 12th and the 15th, `@Release Post Manager` reminds EMs/PMs to draft/submit bugs via `data/release_posts/unreleased/bugs.yml` or performance improvements via  `data/release_posts/unreleased/performance_improvements.yml`
- [ ] By the 16th: `@TW Lead` reviews, applies the `ready` label and assigns to `@Release Post Manager`
- [ ] By the 17th: `@Release Post Manager` merges the MR, prior to content assembly
